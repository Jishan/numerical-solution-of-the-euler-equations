/* **************************************************
	Euler equation using godunov method
************************************************** */

#include "euler.h"

/* ************************************************** */
void solveuler (Grid1D *grid) 
/* ************************************************** */
{
	  Grid1D *pG;
	  pG = grid;
    double timtol=1.0e-06; 
    double time =0.0;
/*    int    ntmaxi=10;	ERROR ======================== */

    int    ntmaxi=100000;
    double cflcoe=0.9;
    double timeou=0.2 ;
	  double	dt;
	  int i;
   
    initVars (pG);     
    printf ("line:%d\n",__LINE__);		
//	  exit(EXIT_FAILURE);  
    printf("\n----------------------------------------\n");
    printf("\n  time step n        time             timeou\n");   
    printf("\n----------------------------------------\n");
    

// ntmaxi = 2;
    
    for(i=1;i <= ntmaxi;i++)
    { 
     
    fillBC (pG);
    dt= timeStep(pG, cflcoe,time,timeou);
    pG->dt = dt;
    time = time + dt;
		grid->time = time;
		printf ("%d\t%g\t%g\n",i,dt,time);		
    fluxGodunov (pG);       
    update (pG);
    

    if ((fabs (time - timeou)) <= timtol)
     {
			printf ("-----------------------------------\n");
			printf ("   Number of time steps = %3d\n", i);
			break;
		 }
	  }
    
   writeSolution (pG, time);

   printf ("\n********** Euler equation: OK ! **********\n\n");
	 return;
}
	
/* ************************************************** */
void initVars (Grid1D *grid) 
/* ************************************************** */
{
    int i;
		int ie = grid->cells + grid->nghost - 1; 
    double xpos;
    ggama = 1.4;      
    g1 = (ggama - 1.0)/(2.0*ggama);
    g2 = (ggama + 1.0)/(2.0*ggama);
    g3 = 2.0*ggama/(ggama - 1.0);
    g4 = 2.0/(ggama - 1.0);
    g5 = 2.0/(ggama + 1.0); 
    g6 = (ggama - 1.0)/(ggama + 1.0);
    g7 = (ggama - 1.0)/2.0;
    g8 = ggama - 1.0;
    
    int	nx = grid->cells + 2*grid->nghost;
    double dx = grid->dx;

    
    grid->W = (Gridprimitive *) calloc (nx, sizeof (Gridprimitive));
	  if (grid->W == NULL) 
	  {
		printf("out of memory\n");
		return;
	  }   
	  
    grid->U = (Gridconservative *) calloc (nx, sizeof (Gridconservative));
	  if (grid->U == NULL) 
	  {
		printf("out of memory\n");
		return;
		
	  }   
	  
	  
	  grid->F = (Gridconservative *) calloc (nx, sizeof (Gridconservative));
	  if (grid->F == NULL) 
	  {
		printf("out of memory\n");
		return;
		
	  }   
	  
    for(i=0; i <= nx; i++)
    {
    xpos = ((double)i - 0.5)*dx;
/*      
     if(xpos <  grid->diaph1)
    {
       grid->W[i].d = 1.0;
       grid->W[i].u = 0.75;
       grid->W[i].p = 1.0;
     }
     
    if(xpos > grid->diaph1 && xpos <  grid->diaph2)
    {    
       grid->W[i].d = 1.0;
       grid->W[i].u = 0.75;
       grid->W[i].p = 1.0;
     }
    if(xpos >  grid->diaph2)
    {   
       grid->W[i].d = 0.125;
       grid->W[i].u = 0.0;
       grid->W[i].p = 0.1;
           
    } 
*/

     if(xpos <  grid->diaph1)
    {
       grid->W[i].d = 1.0;
       grid->W[i].u = 0.75;
       grid->W[i].p = 1.0;
     }
     else if(xpos >  grid->diaph1)
    {   
       grid->W[i].d = 0.125;
       grid->W[i].u = 0.0;
       grid->W[i].p = 0.1;
           
    } 
 
    grid->U[i].d = grid->W[i].d;
    grid->U[i].m = grid->W[i].d *grid->W[i].u;    
    grid->U[i].E= 0.5*grid->U[i].m*grid->W[i].u+grid->W[i].p/g8;     
        }
  return; 
         
   }
   
 //exit(EXIT_FAILURE);  

/* ************************************************** */
void fillBC (Grid1D *grid) 
/* ************************************************** */

{
    int cells = grid->cells;

    if(grid->ibclef == 0)
    {
     grid->W[0].d = grid->W[1].d;
     grid->W[0].u = grid->W[1].u;
     grid->W[0].p = grid->W[1].p ;
     
    }

    else
    {
     grid->W[0].d =  grid->W[1].d;
     grid->W[0].u = -grid->W[1].u;
     grid->W[0].p =  grid->W[1].p;
     }

    if(grid->ibcrig==0)
    {
//printf ("cells=%d\n",cells);
     grid->W[cells + 1].d = grid->W[cells].d;
     grid->W[cells + 1].u = grid->W[cells].u;
     grid->W[cells + 1].p = grid->W[cells].p;
    }
    else
    {
     grid->W[cells + 1].d = grid->W[cells].d;
     grid->W[cells + 1].u =-grid->W[cells].u;
     grid->W[cells + 1].p = grid->W[cells].p;
     }
return;   
 }
/*********************************************************************/   
double timeStep(Grid1D *grid, double cflcoe,double time,	double timeou)
/*********************************************************************/
{
    double smax, sbextd;
    int i; 
    double 	dx = grid->dx;
	  double	dt; 
    smax = 0.0;
    
    for(i=0; i <= grid->cells+1; i++)
    {
    
     {
      grid->W[i].c= sqrt(ggama*(grid->W[i].p/grid->W[i].d));
      sbextd = fabs(grid->W[i].u) + grid->W[i].c;
      }
      if(sbextd > smax)
         smax = sbextd;
    }
   
    dt = cflcoe*dx/smax;
  
   /* if(n <= 5)
    dt = 0.2*dt;
 */
    if((time + dt) > timeou)
    {
     dt = timeou - time;
    }
   return dt;
 }

/*********************************************************************/
void fluxGodunov (Grid1D *grid)
/*********************************************************************/
{
    Grid1D *pG;
   	pG = grid;
/*   	
   	Gridprimitive pR;
   	Gridprimitive pL;
*/ 	   	
    int i;
    double  dsam, energs,pm,psam,um,usam, xovert;
    

     
    xovert = 0.0;	
    for(i=0;i <= grid->cells; i++)
    {    
/*     
     pL.d = grid->W[i].d;
     pL.u = grid->W[i].u;
     pL.p = grid->W[i].p;
     pL.c = grid->W[i].c;
         
     pR.d = grid->W[i+1].d;
     pR.u = grid->W[i+1].u;
     pR.p = grid->W[i+1].p;
     pR.c = grid->W[i+1].c;
*/
/*     
    exact(&pL,&pR,&pm, &um); 
    sample(&pL,&pR,pm, um, xovert, &dsam,&usam, &psam);
*/
//printf ("%d d=%g u=%g p=%g\n",i,grid->W[i].d,grid->W[i].u,grid->W[i].p);
    exact  (grid->W[i], grid->W[i+1],&pm, &um); 
    sample (grid->W[i], grid->W[i+1],pm, um, xovert, &dsam,&usam, &psam);

//printf ("dsam=%g\n",dsam);

    grid->F[i].d= dsam*usam;
    grid->F[i].m= dsam*usam*usam + psam;
    energs = 0.5*usam*usam*dsam + psam/g8;
    grid->F[i].E= usam*(energs + psam);
    }
 }


/*********************************************************************/
void update (Grid1D *grid)
/*********************************************************************/  
{
    int i;
    double	dtodx = grid->dt / grid->dx;
//      printf ("dtodx=%g\n",dtodx);   
    
    for(i=1; i <= grid->cells; i++)
    {

     grid->U[i].d=grid->U[i].d+dtodx*(grid->F[i-1].d-grid->F[i].d);
     grid->U[i].m=grid->U[i].m+dtodx*(grid->F[i-1].m-grid->F[i].m);
     grid->U[i].E=grid->U[i].E+dtodx*(grid->F[i-1].E-grid->F[i].E);
     
    }
         
    for(i=1; i <= grid->cells; i++)
    {
     grid->W[i].d = grid->U[i].d;
     grid->W[i].u = grid->U[i].m/grid->W[i].d;
     grid->W[i].p = g8*(grid->U[i].E- 0.5*grid->U[i].m*grid->W[i].u);
     
     
    }
 return;
 }


/*********************************************************************/     
void	writeSolution (Grid1D *grid,double t)
/*********************************************************************/
{
/*
    int i;
    double energi,xpos;
    double	dx = grid->dx;
    for(i=1; i <= grid->cells; i++)
    {  
    xpos   = ((double)i - 0.5)*dx;
    energi = grid->W[i].p/grid->W[i].d/g8/grid->pscale;
    printf("%g %g %g %g %g\n ",xpos, grid->W[i].d,grid->W[i].u,grid->W[i].p/grid->pscale,energi);        
    }
    return;
*/    
    
	int			i;
	int			is = grid->nghost;
	int			ie = grid->cells+grid->nghost-1;
	double	xpos;
	double	dx = grid->dx;

//	char name[8] = "density";
	char name[9] = "solution";

	FILE *fileout;
	if (!(fileout = fopen (name,"w+"))) {
		printf ("Error! Unable to open file.\n");
	}
	for (i = is; i <= ie; i++) {
    xpos  = ((double) i - 0.5) * dx;
//		fprintf (fileout, "%g\t%g\n", xpos, grid->U[i].d);
		fprintf (fileout, "%g\t%g\t%g\t%g\n", xpos,
				grid->W[i].d, grid->W[i].u, grid->W[i].p);
	}
	fclose (fileout);    
    
  }
/*********************************************************************/

















