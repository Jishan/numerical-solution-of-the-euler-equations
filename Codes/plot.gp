
plot 'solution' u 1:2 t 'density' w p, 'solution' u 1:3  t 'velocity' w p, 'solution' u 1:4 t 'pressure' w p
set term postscript eps enhanced color
set output 'plot.eps'
replot
set output
set terminal x11

