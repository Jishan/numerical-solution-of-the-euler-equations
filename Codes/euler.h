#ifndef EULER_H
#define EULER_H
/* ************************************************** */
/*	Jishan Ahmed																*/
/* ************************************************** */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
/*
typedef struct structGridprimitive {
double dl,ul,pl,cl,dr,ur,pr,cr;
}Gridprimitive;
*/
typedef struct structGridconservative {
double d,m,E;
}Gridconservative;

typedef struct structGridprimitive {
double d,u,p,c;
}Gridprimitive;

typedef struct structGrid1D {
int nfreq,ntmaxi,cells,ibclef,ibcrig,nghost;
double timeou,cflcoe,domlen, diaph1, diaph2;
double pscale,time;
double dt,dx;
Gridprimitive	*W;
Gridconservative *U;
Gridconservative *F;
}Grid1D;

double  ggama;
double  g1,g2,g3,g4,g5,g6,g7,g8;

void		initGrid (Grid1D *grid);
void		initVars (Grid1D *grid);
void		fillBC (Grid1D *grid);
void		solveuler (Grid1D *grid) ;
double  timeStep(Grid1D *grid,double cflcoe,double time,double timeou);
void		update (Grid1D *grid);
void		writeSolution (Grid1D *grid,double t);
void	  fluxGodunov (Grid1D *grid);

void    exact(Gridprimitive pL,Gridprimitive pR,double *p,double *u);
void    guessp(Gridprimitive pL,Gridprimitive pR,double *pm);

void 		prefun(Gridprimitive pK,double *f,double *fd,double p);

void    sample(Gridprimitive pL,Gridprimitive pR,double pm, double um, double s, double *d, double *u, double *p);

#endif
