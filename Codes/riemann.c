# include "euler.h"
/*********************************************************************/  
void exact(Gridprimitive pL, Gridprimitive pR, double *p, double *u)
/*********************************************************************/
  {
  
    const int nriter = 20;
    const double tolpre = 1.0e-5;
    double change, fl, fld, fr, frd, pold, pstart, udiff;
    int i;
    //printf("11 STARPU PM %12.7f  STARPU UM %12.7f\n",*p,*u); 
//    printf ("line=%d pstart=%g\n",__LINE__,pstart);
    guessp(pL, pR, &pstart);
    
//    printf ("line=%d pstart=%g\n",__LINE__,pstart);
    
    pold  = pstart;
    
    udiff = pR.u - pL.u;
    
    for(i=1; i <=nriter; i++)
    {


//printf ("i=%d fl=%g fr=%g fld=%g frd=%g pold=%g udiff=%g\n",i,fl,fr, fld,frd,pold,udiff);
     prefun(pL, &fl, &fld, pold);
//     printf ("i=%d fl=%g fld=%g  pold=%g udiff=%g\n",i,fl,fld,pold,udiff);
  
   
     prefun(pR, &fr, &frd, pold);
     
//printf ("i=%d fl=%g fr=%g fld=%g frd=%g pold=%g udiff=%g\n",i,fl,fr, fld,frd,pold,udiff);     
     
     *p= pold -(fl + fr + udiff)/(fld + frd);
     change = 2.0*fabs(((*p) - pold)/(*p + pold));
     
//printf ("i=%d *p=%g change=%g\n",i,*p,change);     
     
//printf ("i=%d change=%g\n",i,change);     
      if(change < tolpre)
         break;
       if(*p < 0.0)
/*	ERROR ============================
       {
          *p = tolpre;
          pold  = *p;
       }
*/
       {
          *p = tolpre;
       }
       pold  = *p;

      }
      if( i > nriter)
      printf("\nDivergence in Newton-Raphson iteration\n");

      *u = 0.5*(pL.u +  pR.u + fr - fl);
//printf ("u=%g\n",*u);
 }

/*********************************************************************/
void guessp(Gridprimitive pL, Gridprimitive pR, double *pm)
/*********************************************************************/
 {
//   printf ("line=%d pstart=%g\n",__LINE__,*pm);
    double cup,gel,ger,pmax, pmin, ppv,pq;
    double ptl,ptr,qmax,quser,um;

//printf ("56 dl=%g ul=%g pl=%g dr=%g ur=%g pr=%g\n", pL.d,pL.u,pL.p,pR.d,pR.u,pR->p);

    quser = 2.0;
    cup  = 0.25*(pL.d +pR.d)*(pL.c + pR.c);
    ppv  = 0.5*(pL.p + pR.p) + 0.5*(pL.u -pR.u)*cup;
    ppv  = fmax(0.0,ppv);
    pmin = fmin(pL.p,pR.p);
    pmax = fmax(pL.p,pR.p);
    qmax = pmax/pmin;  

//printf ("cup=%g ppv=%g pmin=%g pmax=%g qmax=%g\n",cup, ppv, pmin, pmax, qmax);

    if(qmax <= quser && (pmin <= ppv && ppv <= pmax))
   *pm = ppv;
    
    else{
         if(ppv < pmin)
          {
            pq  = pow(pL.p/pR.p,g1);
            um  = (pq*pL.u/pL.c +pR.u/pR.c +g4*(pq - 1.0))/(pq/pL.c + 1.0/pR.c);
            ptl = 1.0 +g7*(pL.u - um)/pL.c;
            ptr = 1.0 + g7*(um - pR.u)/pR.c;
            *pm  = 0.5*(pow(pL.p*ptl,g3)+ pow(pR.p*ptr,g3));
           }
         else{

            gel = sqrt((g5/pL.d)/(g6*pL.p + ppv));
            ger = sqrt((g5/pR.d)/(g6*pR.p + ppv));
            *pm  = (gel*pL.p + ger*pR.p - (pR.u - pL.u))/(gel + ger);
            }
        }
 // printf ("87 dl=%g ul=%g pl=%g dr=%g ur=%g pr=%g\n", pL.d,pL.u,pL.p,pR.d,pR.u,pR.p);      
        
  }
/*********************************************************************/
void prefun(Gridprimitive pK,double *f,double *fd,double p)
/*********************************************************************/
{

    double ak,bk,pratio,qrt;
   
      if(p < (pK.p))
      {
         pratio = p/(pK.p);
         *f = g4*(pK.c)*(pow(pratio,g1) - 1.0);
         *fd = (1.0/((pK.d)*(pK.c)))*pow(pratio,(-g2));
       }
      else
      {
         ak  = g5/(pK.d);
         bk  = g6*(pK.p);
         qrt = sqrt(ak/(bk + p));
         *f   = (p- (pK.p))*qrt;
         *fd  = (1.0 - 0.5*(p-(pK.p))/(bk +p))*qrt;
       }
 }
 
/*********************************************************************/
  void sample(Gridprimitive pL,Gridprimitive pR,double pm, double um, double s, double *d, double *u, double *p)
/*********************************************************************/ 
{
  
    double cml, cmr,pml, pmr;
    double c,shl, shr, sl, sr, stl, str;
     
    if(s <= um)
    {

         if(pm <=pL.p)
         {

            shl = pL.u - pL.c;

            if(s <= shl)
            {

               *d = pL.d;
               *u = pL.u;
               *p = pL.p;
               }
            else
             {
               {
               cml = pL.c*pow((pm/pL.p),g1);
               stl = um - cml;
               }
               
               if(s > stl)
               {

                  *d = pL.d*pow(pm/pL.p ,1.0/ggama);
                  *u = um;
                  *p = pm;
                  }
               else
                {
                 *u = g5*(pL.c +g7*pL.u + s);
                  c = g5*(pL.c +g7*(pL.u-s));
                 *d = pL.d*pow((c/pL.c),g4);
                 *p = pL.p*pow((c/pL.c),g3);
                }
            }
          }
         else
         {
           {
            pml = pm/ pL.p;
            sl  = pL.u -pL.c*sqrt(g2*pml + g1);
            }

            if(s <= sl)
            {
               *d =pL.d;
               *u =pL.u;
               *p =pL.p;
             }

            else
            {
               *d = pL.d*(pml +g6)/(pml*g6 + 1.0);
               *u = um;
               *p = pm;
            }
          }
      }     
      else
      {

         if(pm > pR.p)
         {
           pmr = pm/pR.p;
           sr  =pR.u +pR.c*sqrt(g2*pmr + g1); 
 
            if(s >= sr)
             {
              *d = pR.d;
              *u = pR.u;
              *p = pR.p;
               }
            else
            {
               *d = pR.d*(pmr +g6)/(pmr*g6 + 1.0);
               *u = um;
               *p = pm;
             }
          }
         else
         {
            shr = pR.u + pR.c;

            if(s >= shr)
              {
               *d =pR.d;
               *u =pR.u;
               *p =pR.p;
               }
            else
             {
               {
               cmr = pR.c*pow((pm/pR.p),g1);
               str = um + cmr;
                }
               if(s <= str)
               {
                  *d = pR.d*pow(pm/pR.p,1.0/ggama);
                  *u = um;
                  *p = pm;
                }
               else
                 {
                 *u = g5*(-pR.c+g7*pR.u + s);
                  c = g5*(pR.c -g7*(pR.u - s));
                 *d = pR.d*pow((c/pR.c),g4);
                 *p = pR.p*pow((c/pR.c),g3);
                  }
               }
           }
        }
   }
/*********************************************************************/
