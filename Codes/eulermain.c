# define MAIN_C
/*
Jishan
                                                    */
 
# include "euler.h"

/* ************************************************** */
int main () {
/* ************************************************** */

	Grid1D grid;
	initGrid (&grid);
	solveuler(&grid);
	return 0;
}
/* ************************************************** */
void initGrid (Grid1D *grid)
/* ************************************************** */
{
    grid->domlen= 1.0;
    grid->diaph1=0.3;
    grid->diaph2=0.3 ;
    grid->cells=100;
    grid->nghost=1;
  
    //grid->timeou=0.2 ;
    
    grid->ibclef=0;
    grid->ibcrig=0;
    //grid->nfrequ=10 ;
    grid->dx= 1.0/(double)grid->cells;
    grid-> pscale=1.0e+00;
}
/* ************************************************** */




